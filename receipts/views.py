from django.shortcuts import render, redirect 
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import CreateReceipt, CreateExpenseCategoryForm, CreateAccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def receipts(request):
  receipts = Receipt.objects.filter(purchaser=request.user)
  context = {"receipts": receipts }
  return render(request, "receipts/receipts.html", context) 

@login_required
def create_receipt(request):
  if request.method == "POST":
    form = CreateReceipt(request.POST)
    
    if form.is_valid():
      receipt = form.save(False) # Not saved automatically
      receipt.purchaser = request.user # login required
      receipt.save()
      return redirect("home") #homepage
  else:
    form = CreateReceipt()

  context = {"form":form}

  return render(request, "receipts/create.html", context) #create recipe

@login_required
def category_list(request):
  categories = ExpenseCategory.objects.filter(owner = request.user)  
  context = {"categories":categories}

  return render(request, "categories/categories_list.html", context)

@login_required
def account_list(request):
  accounts = Account.objects.filter(owner = request.user)
  context = { "accounts": accounts}

  return render(request, "accounts/account_list.html", context)

@login_required
def create_category(request):
  if request.method == "POST":
    form = CreateExpenseCategoryForm(request.POST)
    if form.is_valid():
      category = form.save(False)
      category.owner = request.user
      category.save()
      return redirect("category_list")
    
  else:
    form = CreateExpenseCategoryForm()

  context = {"form":form}
  return render(request, "categories/create_category.html", context)

@login_required
def create_account(request):
  if request.method == "POST":
    form = CreateAccountForm(request.POST)
    if form.is_valid():
      category = form.save(False)
      category.owner = request.user
      category.save()
      return redirect("account_list")
    
  else:
    form = CreateAccountForm()

  context = {"form":form}
  return render(request, "accounts/create_account.html", context)
